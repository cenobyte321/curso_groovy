println "ejemplo"
s = "Hola mundo"
println s

lista = new ArrayList()
println lista.getClass().getName()

//Método mágico para toString de los atributos
//@groovy.transform.ToString
class User{
	String name 
	Date fechaDeNacimiento
	Profile profile
	//return no es necesario, es como Ruby
	String saluda(){
		"Hola $name"
	}
	String toString(){
		name + " " + fechaDeNacimiento
		//Interpolación de variables
		"$name $fechaDeNacimiento -> ${saluda()} ${1*3*4}"
	}
}
@groovy.transform.ToString
@groovy.transform.TupleConstructor
class Profile{
	String twitter
	String github
}

User u = new User()
u.setName("Juan")
//Hoy menos X días
u.setFechaDeNacimiento(new Date()-(365*25))

//Sin TupleConstructor
//u.profile = new Profile(twitter: "cenobyte321", github: "cenobyte321")
//Con TupleConstructor
u.profile = new Profile("cenobyte321", "cenobyte321")
println u.profile
println u;


m = []
println m 
m = [:]
println m.getClass() //LinkedHashMap

//Todo es un objeto
1.toString()


//java -cp $GROOVY_HOME/embeddable/groovy-all-2.4.5.jar
assert true;

//Esto da un error de assert:
//assert false

//esto también
//assert 3*4*4+54*5 == 4*5*3+4*5

def n = "" // == false
n = "sdij" //true
n = 0 //false
n = null //false 

assert !n

n = "hola"
if(n != null && n.size() > 0){
	println n
}

//Si consideramos lo anterior entonces podemos reducir el if
if(n != null && n.size()){
	println n
}

if(n){
	println n
}

n = null
println n?.size().toString();

//CLOSURES
def closure(){
	println "HOLAAAAA"
}

println closure()


def doble(n){
	n * 2
}
def triple(n){
	n * 3
}
println(doble(3))
println(triple(4))


def multiplo(n, closure){
	closure(n)
}

//it == parámetro
println(multiplo(3, { it * 6 }));
println multiplo(3) { it * 6 }
println multiplo(3) { w -> w * 6 }
//Los objetos enviados a closure son pasados como referencia (¡¡AGUAS!!)

def validador = { s -> s == s.reverse() }
def determinarPalabra(s, esPalindrome){
	if(esPalindrome(s)){
		println "Correcto"
	}
	else{
		println "Incorrecto"
	}
}
def palabra = "una palabra"
determinarPalabra(palabra, validador)
determinarPalabra("radar", validador)

arreglo = []
arreglo = [1,2,3,4,5]
arreglo = (1..10)
arreglo = ('a'..'z')
//Esto regresa la lista
arreglo.each { println it*3 }

for(a in arreglo){
	println a
}
arreglo = [2,2,1,4]
arreglo.add("uno")
arreglo << "dos" << "tres" << ["cuatro", 4, "cinco"]
otraLista = [1,2,3,4]
arreglo = arreglo + otraLista
arreglo = arreglo - [1,2]

println arreglo.findAll({it.class == String || it.class == ArrayList})

arreglo = (arreglo*2).flatten()
println arreglo

//Mapas
a = [uno: 1, dos: 2, tres:3]
println a